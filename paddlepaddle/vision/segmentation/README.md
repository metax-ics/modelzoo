# vision/segmentation paddlemodels

## 模型精度支持

|models|fp16|int8|
|:---:|:---:|:---:|
|bisenetv1|√|√|
|bisenetv2|√|√|
|fcn|√|√|
|pspnet|√|√|
|unet|√|√|
|upernet|√|√|

## 数据集和模型下载
```
# 数据集下载
进入paddlepaddle/vision/segmentation/data目录下， 运行./citys_data_prepare.sh

# 部署模型下载
1. 请自行前往driver下载所需模型： https://pub-docstore.metax-tech.com:7001/sharing/8IlMh7DTe
2. 将模型压缩包放置对应模型文件夹内，并解压
```

## demo运行

```
cd paddlepaddle/vision/segmentation
# /run.sh ${model_path}
./run.sh models/pd_bisenetv1_512x1024

# demo参数说明
model_path:   存放模型的文件夹，包含模型cfg和部署模型
```
 