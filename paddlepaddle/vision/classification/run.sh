export MXSDK_LOG_LEVEL=1
export MACA_QUANTIZER_USING_CUDA=0
export PYTHONDONTWRITEBYTECODE=1

MODEL_PATH=$1


if [ $MODEL_PATH == "models/pd_efficientnetb0_224x224" -o $MODEL_PATH == "models/pd_hrnet_w18c_ssld_224x224" -o $MODEL_PATH == "models/pd_mobilenetv3_224x224" -o $MODEL_PATH == "models/pd_pplc_x10_224x224" ] || [ $MODEL_PATH == "models/pd_efficientnetb0_224x224/" -o $MODEL_PATH == "models/pd_hrnet_w18c_ssld_224x224/" -o $MODEL_PATH == "models/pd_mobilenetv3_224x224/" -o $MODEL_PATH == "models/pd_pplc_x10_224x224/" ]; then
    python code/start.py ${MODEL_PATH} 1 fp16 normal

else 
    python code/start.py ${MODEL_PATH} 1 int8 normal
fi