# NLP paddlemodels

## 模型精度支持

|models|fp16|int8|
|:---:|:---:|:---:|
|bert_sst2|√|×|

## 数据集和模型下载
```
# 数据集下载
1. 已放置在data目录下

# 部署模型下载 
1. 请自行前往driver下载所需模型： https://pub-docstore.metax-tech.com:7001/sharing/8IlMh7DTe
2. 将模型压缩包放置对应模型文件夹内，并解压
```

## demo运行
```
cd paddlepaddle/nlp
# ./run.sh ${model_path}
./run.sh models/pd_bert_sst2_64

# demo参数说明
model_path:   存放模型的文件夹，包含模型cfg和部署模型
```
 