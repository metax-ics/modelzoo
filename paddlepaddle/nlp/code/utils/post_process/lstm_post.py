import yaml

import torch
from torch.utils.data import Dataset, DataLoader
import numpy as np

# import kaldiio
import logging
logging.basicConfig(level=logging.DEBUG #设置日志输出格式
                    ,filename="./LOGGING_TRT.log" #log日志输出的文件位置和文件名
                    # ,filemode="w" #文件的写入格式，w为重新写入文件，默认是追加
                    ,format="%(asctime)s - %(name)s - %(levelname)-9s: %(message)s" #日志输出的格式  - %(filename)-8s   - %(lineno)s line - 
                    # -8表示占位符，让输出左对齐，输出长度都为8位
                    ,datefmt="%Y-%m-%d %H:%M:%S" #时间输出的格式
                    )


class Config(object):
    batch_size = 1
    dropout = 0.1

class Decoder(object):
    "解码器基类定义，作用是将模型的输出转化为文本使其能够与标签计算正确率"
    def __init__(self, int2char, space_idx = 1, blank_index = 0):
        '''
        int2char     :     将类别转化为字符标签
        space_idx    :     空格符号的索引，如果为为-1，表示空格不是一个类别
        blank_index  :     空白类的索引，默认设置为0
        '''
        self.int_to_char = int2char
        self.space_idx = space_idx
        self.blank_index = blank_index
        self.num_word = 0
        self.num_char = 0

    def decode(self):
        "解码函数，在GreedyDecoder和BeamDecoder继承类中实现"
        raise NotImplementedError;

    def phone_word_error(self, prob_tensor, frame_seq_len, targets, target_sizes):
        '''计算词错率和字符错误率
        Args:
            prob_tensor     :   模型的输出
            frame_seq_len   :   每个样本的帧长
            targets         :   样本标签
            target_sizes    :   每个样本标签的长度
        Returns:
            wer             :   词错率，以space为间隔分开作为词
            cer             :   字符错误率
        '''
        strings = self.decode(prob_tensor, frame_seq_len)
        targets = self._unflatten_targets(targets, target_sizes)
        target_strings = self._process_strings(self._convert_to_strings(targets))
        
        cer = 0
        wer = 0
        for x in range(len(target_strings)):
            cer += self.cer(strings[x], target_strings[x])
            wer += self.wer(strings[x], target_strings[x])
            self.num_word += len(target_strings[x].split())
            self.num_char += len(target_strings[x])
        return cer, wer

    def _unflatten_targets(self, targets, target_sizes):
        '''将标签按照每个样本的标签长度进行分割
        Args:
            targets        :    数字表示的标签
            target_sizes   :    每个样本标签的长度
        Returns:
            split_targets  :    得到的分割后的标签
        '''
        split_targets = []
        offset = 0
        for size in target_sizes:
            split_targets.append(targets[offset : offset + size])
            offset += size
        return split_targets

    def _process_strings(self, seqs, remove_rep = False): 
        '''处理转化后的字符序列，包括去重复等，将list转化为string
        Args:
            seqs       :   待处理序列
            remove_rep :   是否去重复
        Returns:
            processed_strings  :  处理后的字符序列
        '''
        processed_strings = []
        for seq in seqs:
            string = self._process_string(seq, remove_rep)
            processed_strings.append(string)
        return processed_strings
   
    def _process_string(self, seq, remove_rep = False):
        string = ''
        for i, char in enumerate(seq):
            if char != self.int_to_char[self.blank_index]:
                if remove_rep and i != 0 and char == seq[i - 1]: #remove dumplicates
                    pass
                elif self.space_idx == -1:
                    string = string + ' '+ char
                elif char == self.int_to_char[self.space_idx]:
                    string += ' '
                else:
                    string = string + char
        return string

    def _convert_to_strings(self, seq, sizes=None):
        '''将数字序列的输出转化为字符序列
        Args:
            seqs       :   待转化序列
            sizes      :   每个样本序列的长度
        Returns:
            strings  :  转化后的字符序列
        '''
        strings = []
        for x in range(len(seq)):
            seq_len = sizes[x] if sizes is not None else len(seq[x])
            string = self._convert_to_string(seq[x], seq_len)
            strings.append(string)
        return strings

    def _convert_to_string(self, seq, sizes):
        result = []
        for i in range(sizes):
            result.append(self.int_to_char[seq[i]])
        if self.space_idx == -1:
            return result
        else:
            return ''.join(result)
 
    def wer(self, s1, s2):
        "将空格作为分割计算词错误率"
        b = set(s1.split() + s2.split())
        word2int = dict(zip(b, range(len(b))))

        w1 = [word2int[w] for w in s1.split()]
        w2 = [word2int[w] for w in s2.split()]
        return self._edit_distance(w1, w2)

    def cer(self, s1, s2):
        "计算字符错误率"
        return self._edit_distance(s1, s2)
    
    def _edit_distance(self, src_seq, tgt_seq):
        "计算两个序列的编辑距离，用来计算字符错误率"
        L1, L2 = len(src_seq), len(tgt_seq)
        if L1 == 0: return L2
        if L2 == 0: return L1
        # construct matrix of size (L1 + 1, L2 + 1)
        dist = [[0] * (L2 + 1) for i in range(L1 + 1)]
        for i in range(1, L2 + 1):
            dist[0][i] = dist[0][i-1] + 1
        for i in range(1, L1 + 1):
            dist[i][0] = dist[i-1][0] + 1
        for i in range(1, L1 + 1):
            for j in range(1, L2 + 1):
                if src_seq[i - 1] == tgt_seq[j - 1]:
                    cost = 0
                else:
                    cost = 1
                dist[i][j] = min(dist[i][j-1] + 1, dist[i-1][j] + 1, dist[i-1][j-1] + cost)
        return dist[L1][L2]

class GreedyDecoder(Decoder):
    "直接解码，把每一帧的输出概率最大的值作为输出值，而不是整个序列概率最大的值"
    def decode(self, prob_tensor, frame_seq_len):
        '''解码函数
        Args:
            prob_tensor   :   网络模型输出
            frame_seq_len :   每一样本的帧数
        Returns:
            解码得到的string，即识别结果
        '''
        prob_tensor = prob_tensor.transpose(0,1)
        _, decoded = torch.max(prob_tensor, 2)
        decoded = decoded.view(decoded.size(0), decoded.size(1))
        decoded = self._convert_to_strings(decoded.numpy(), frame_seq_len)
        return self._process_strings(decoded, remove_rep=True)

class Vocab(object):
    def __init__(self, vocab_file):
        self.vocab_file = vocab_file
        self.word2index = {"blank": 0, "UNK": 1}
        self.index2word = {0: "blank", 1: "UNK"}
        self.word2count = {}
        self.n_words = 2
        self.read_lang()

    def add_sentence(self, sentence):
        for word in sentence.split(' '):
            self.add_word(word)

    def add_word(self, word):
        if word not in self.word2index:
            self.word2index[word] = self.n_words
            self.word2count[word] = 1
            self.index2word[self.n_words] = word
            self.n_words += 1
        else:
            self.word2count[word] += 1

    def read_lang(self):
        print("Reading vocabulary from {}".format(self.vocab_file))
        with open(self.vocab_file, 'r') as rf:
            line = rf.readline()
            while line:
                line = line.strip().split(' ')
                if len(line) > 1:
                    sen = ' '.join(line[1:])
                else:
                    sen = line[0]
                self.add_sentence(sen)
                line = rf.readline()
        print("Vocabulary size is {}".format(self.n_words))

def make_context(feature, left, right):
    if left==0 and right == 0:
        return feature
    feature = [feature]
    for i in range(left):
        feature.append(np.vstack((feature[-1][0], feature[-1][:-1])))
    feature.reverse()
    for i in range(right):
        feature.append(np.vstack((feature[-1][1:], feature[-1][-1])))
    return np.hstack(feature)

def skip_feat(feature, skip):
    '''
    '''
    if skip == 1 or skip == 0:
        return feature
    skip_feature=[]
    for i in range(feature.shape[0]):
        if i % skip == 0:
            skip_feature.append(feature[i])
    return np.vstack(skip_feature)

class SpeechDataset(Dataset):
    def __init__(self, vocab, scp_path, lab_path, opts):
        self.vocab = vocab
        self.scp_path = scp_path
        self.lab_path = lab_path
        self.left_ctx = opts.left_ctx
        self.right_ctx = opts.right_ctx
        self.n_skip_frame = opts.n_skip_frame
        self.n_downsample = opts.n_downsample
        self.feature_type = opts.feature_type
        self.mel = opts.mel

        self.process_feature_label()
    
    def process_feature_label(self):
        path_dict = []
        #read the ark path
        with open(self.scp_path, 'r') as rf:
            line = rf.readline()
            while line:
                utt, path = line.strip().split(' ')
                path = path.replace("data/dev/","data/lstm_data/dev/")
                path_dict.append((utt, path))
                line = rf.readline()
        
       	#read the label
        label_dict = dict()
        with open(self.lab_path, 'r') as rf:
            line = rf.readline()
            while line:
                utt, label = line.strip().split(' ', 1)
                label_dict[utt] = [self.vocab.word2index[c] if c in self.vocab.word2index else self.vocab.word2index['UNK'] for c in label.split()]
                line = rf.readline() 
        
        assert len(path_dict) == len(label_dict)
        print("Reading %d lines from %s" % (len(label_dict), self.lab_path))
        
        self.item = []
        for i in range(len(path_dict)):
            utt, path = path_dict[i]
            self.item.append((path, label_dict[utt], utt))

    def __getitem__(self, idx):

        path, label, utt = self.item[idx]
        # feat = kaldiio.load_mat(path)
        feat = np.load("data/lstm_data/dev/fbank/feat{}.npy".format(idx))
        # np.save("data/lstm_data/dev/fbank/feat{}.npy".format(idx),feat)
        feat= skip_feat(make_context(feat, self.left_ctx, self.right_ctx), self.n_skip_frame)
        seq_len, dim = feat.shape
        if seq_len % self.n_downsample != 0:
            pad_len = self.n_downsample - seq_len % self.n_downsample
            feat = np.vstack([feat, np.zeros((pad_len, dim))])

        return (torch.from_numpy(feat), torch.LongTensor(label), utt)

    def __len__(self):
        return len(self.item) 

def create_input(batch):
    inputs_max_length = 390
    feat_size = batch[0][0].size(1)
    targets_max_length = 74
    batch_size = len(batch)
    batch_data = torch.zeros(batch_size, inputs_max_length, feat_size) 
    batch_label = torch.zeros(batch_size, targets_max_length)
    input_sizes = torch.zeros(batch_size)
    target_sizes = torch.zeros(batch_size)
    utt_list = []

    for x in range(batch_size):
        feature, label, utt = batch[x]
        feature_length = feature.size(0)
        label_length = label.size(0)

        batch_data[x].narrow(0, 0, feature_length).copy_(feature)
        batch_label[x].narrow(0, 0, label_length).copy_(label)
        input_sizes[x] = feature_length / inputs_max_length
        target_sizes[x] = label_length
        utt_list.append(utt)
    return batch_data.float(), input_sizes.float(), batch_label.long(), target_sizes.long(), utt_list 
    
'''
class torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=False, sampler=None, batch_sampler=None, num_workers=0, 
                                                    collate_fn=<function default_collate>, pin_memory=False, drop_last=False)
subclass of DataLoader and rewrite the collate_fn to form batch
'''

class SpeechDataLoader(DataLoader):
    def __init__(self, *args, **kwargs):
        super(SpeechDataLoader, self).__init__(*args, **kwargs)
        self.collate_fn = create_input

def get_lstm_post(outputs, batch_size, params, content):

    # output_size = tuple([int(size) for size in params["outputs_size"].split(",")])
    height, width = input_size.split(",")
    outputs_sizes_list = [[int(size) for size in output_size.split(',')] for output_size in outputs_size]
    print(outputs_sizes_list)
    conf = yaml.safe_load(open("./data/lstm_label.yaml",'r'))

    opts = Config()
    for k,v in conf.items():
        setattr(opts, k, v)
        print('{:50}:{}'.format(k, v))

    # beam_width = opts.beam_width
    vocab_file = opts.vocab_file

    vocab = Vocab(vocab_file)
    test_dataset = SpeechDataset(vocab, opts.valid_scp_path, opts.valid_lab_path, opts)
    test_loader = SpeechDataLoader(test_dataset, batch_size=1, shuffle=False, num_workers=4, pin_memory=False)

    decoder  = GreedyDecoder(vocab.index2word, space_idx=-1, blank_index=0)


    total_wer = 0
    total_cer = 0
    test_num = 399 // 1
    for i, data in zip(range(test_num), test_loader):
        inputs, input_sizes, targets, target_sizes, utt_list = data
        # probs_1_np  = np.load('./data/lstm_onnx_infer/outputs_{}.npy'.format(i))
        probs_1_np  = tensors_list[i][0].reshape(outputs_sizes_list[0])
        probs_1 = torch.from_numpy(probs_1_np)

        max_length = probs_1.size(0)
        input_sizes = (input_sizes * max_length).long()

        decoded = decoder.decode(probs_1, input_sizes.numpy().tolist())
        targets, target_sizes = targets.numpy(), target_sizes.numpy()
        labels = []
        for i in range(len(targets)):
            label = [ vocab.index2word[num] for num in targets[i][:target_sizes[i]]]
            labels.append(' '.join(label))

        # for x in range(len(targets)):
        #     print("origin : " + labels[x])
        #     print("decoded: " + decoded[x])
        cer = 0
        wer = 0
        for x in range(len(labels)):
            cer += decoder.cer(decoded[x], labels[x])
            wer += decoder.wer(decoded[x], labels[x])
            decoder.num_word += len(labels[x].split())
            decoder.num_char += len(labels[x])
        total_cer += cer
        total_wer += wer

    CER = (float(total_cer) / decoder.num_char)*100
    WER = (float(total_wer) / decoder.num_word)*100
    print("Character error rate on test set: %.4f" % CER)
    print("Word error rate on test set: %.4f" % WER)
    logging.info("NLP_model_ox_lstm_390x243_bs1_prec{}   ORT-CER: {:.4f}".format(precision, CER))
    logging.info("NLP_model_ox_lstm_390x243_bs1_prec{}   ORT-WER: {:.4f}".format(precision, WER))


# if __name__ == '__main__':
#     get_lstm_post(None, 1, None)