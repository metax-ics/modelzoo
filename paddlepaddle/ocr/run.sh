export MXSDK_LOG_LEVEL=1
export MACA_QUANTIZER_USING_CUDA=0
export PYTHONDONTWRITEBYTECODE=1

MODEL_PATH=$1

if [ $MODEL_PATH == "models/pd_crnn_rec_32x100" ] || [ $MODEL_PATH == "models/pd_crnn_rec_32x100/" ]; then
    python code/start.py ${MODEL_PATH} 1 fp16 normal

else 
    python code/start.py ${MODEL_PATH} 1 int8 normal

fi