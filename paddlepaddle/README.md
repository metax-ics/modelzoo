# Paddlepaddle模型库
目前支持paddlepaddle模型有图像分类/检测/人脸/分割、OCR和NLP，3大类共50个模型，后续会继续增加模型类别和个数


## 测试环境准备
```
1.配置好硬件显卡的driver相应驱动，可按照发布手册自行配置
2.按照发布手册安装onnxruntime-macavx, maca-quantizer, maca-converter内置whl包
3.安装测试运行依赖
    pip install -r requirements.txt
```

## paddle模型测试流程 

### 1.数据集及模型准备
请按相应任务内的readme.md说明下载对应数据集及模型


### 2.模型推理测试
```
# 分类 pd_resnet50_vd_ssld_224x224
cd paddlepaddle/vision/classification
./run.sh models/pd_resnet50_vd_ssld_224x224

# 检测 pd_yolov3_608x608
cd paddlepaddle/vision/detection
./run.sh models/pd_yolov3_608x608

# 人脸 pd_blazeface_1000x1000
cd paddlepaddle/vision/facedetection
./run.sh models/pd_blazeface_1000x1000

# 分割 pd_upernet_512x1024
cd paddlepaddle/vision/segmentation
./run.sh models/pd_upernet_512x1024

# ocr pd_dbmbv3_736x1280
cd paddlepaddle/ocr
./run.sh models/pd_dbmbv3_736x1280  

# nlp pd_bert_sst2_64
cd paddlepaddle/nlp
./run.sh models/pd_bert_sst2_64
```

## 支持模型列表概览

### 图像分类
| 模型| 精度类型 | input-size| CPU参考指标-top1 | 硬件精度 | 内存 | 硬件利用率 | 
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|densenet121|int8|224x224|75.7%|75.2%|348.6MB|98%|
|r50_vd_ssld|int8|224x224|83.0%|82.2%|428.6MB|97%|
|r101_vd_ssld|int8|224x224|83.7%|83.4%|524.6.3MB|91%|
|inceptionv4|int8|299x299|80.8%|80.6%|662.6MB|99%|
|mobilenetv2|int8|224x224|72.2%|71.7%|316.6MB|93%|
|x50_vd_32x4d|int8|224x224|79.6%|79.4%|428.6MB|95%|
|squeezenet11|int8|224x224|60.1%|59.8%|300.6MB|93%|
|alexnet|int8|224x224|56.8%|56.6%|684.6MB|98%|
|darknet|int8|256x256|78.0%|77.5%|462.6MB|97%|
|efficientnetB0|fp16|224x224|77.2%|77.2%|460.6MB|99%|
|goolenet|int8|224x224|70.6%|70.3%|332.6MB|92%|
|hrnet_w18c|fp16|224x224|81.2%|81.2%|716.6MB|99%|
|inceptionv3|int8|299x299|79.1%|78.9%|486.6MB|96%|
|mobilenetv1|int8|224x224|71.0%|70.2%|332.6MB|91%|
|mobilenetv3|fp16|224x224|71.2%|71.2%|354.6MB|94%|
|pphg_ssld|int8|224x224|83.6%|83.3%|642.6MB|99%|
|pphg_tiny|int8|224x224|79.3%|78.9%|386.6MB|99%|
|pplc_x1.0|fp16|224x224|71.3%|71.3%|354.6MB|99%|
|pplcnet_v2|int8|224x224|77.0%|76.5%|338.6MB|86%|
|r18_vd|int8|224x224|72.3%|71.6%|354.6MB|84%|
|r34_vd|int8|224x224|75.9%|75.8%|418.6MB|85%|
|x101_vd_32x4d|int8|224x224|80.3%|80.2%|482.6MB|98%|
|ser50_vd|int8|299x299|79.5%|78.8%|450.6MB|99%|
|shufflenetv2|int8|224x224|71.6%|71.1%|338.6MB|98%|
|vgg16|int8|224x224|72.0%|72.0%|1106.6MB|99%|
|vgg19|int8|224x224|72.5%|72.4%|1058.6MB|98%|
|xception41|int8|224x224|79.2%|79.0%|502.6MB|99%|

### 目标检测
| 模型| 精度类型 | input-size| CPU参考指标-mAP | 硬件精度 | 内存 | 硬件利用率 |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|yolov3|int8|608x608|39.1|38.6|1123.3MB|88%|
|ppyolotiny|int8|320x320|20.6|18.6|574.6MB|84%|
|fcos|fp16|800x800|37.5|37.5|1822.6MB|99%|
|ppyolo|int8|512x512|28.5|26.9|438.6MB|56%|
|ppyoloe|int8|640x640|43.6|42.7|498.6MB|71%|
|retinanet|int8|800x800|35.5|34.7|1456.7MB|85%|
|ssd_mbv1|int8|300x300|73.1|72.5|356.6MB|84%| 
|yolov3_mbv1|int8|608x608|29.5|27.8|734.6MB|83%|
|yolov6s|int8|640x640|44.6|41.9|1082.3MB|73%|
|yolov7_tiny|int8|640x640|37.3|35.6|974.6MB|99%|
|yolov8n|int8|640x640|37.3|35.4|554.6MB|95%|

### 人脸检测
| 模型| 精度类型 | input-size| CPU参考指标-mAP | 硬件精度 | 内存 | 硬件利用率 |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|blazeface|int8|1000x1000|82.8/81.4/65.2|82.1/80.6/63.9|716.6MB|99%|

### 目标分割
| 模型| 精度类型 | input-size| CPU参考指标-mIOU | 硬件精度 | 内存 | 硬件利用率 |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|bisenetv1|int8|512x1024|72.1|71.5|662.6MB|99%|
|bisenetv2|int8|512x1024|65.0|64.7|574.6MB|99%|
|fcnnet|int8|512x1024|74.1|73.6|662.6MB|99%|
|pspnet|int8|512x1024|75.8|74.1|1446.6MB|99%|
|unet|int8|512x1024|65.0|64.9|1494.6MB|99%|
|upernet|int8|512x1024|76.4|75.8|3398.7MB|99%|

### OCR
| 模型| 精度类型 | input-size| CPU参考指标-Hmean/Acc | 硬件精度 | 内存 | 硬件利用率 |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|det_mv3_db_v2|int8|736x1280|75.12|74.8|850.6MB|87%|
|det_ocrv2_ch|int8|736x1280|45.8|43.8|594.6MB|97%|
|rec_crnn|fp16|32x100|89.7|89.7|744.6MB|83%|
|rec_ocrv3_en|int8|48x320|83.3|82.5|312.6.3MB|69%|

### NLP
| 模型| 精度类型 | input-size| CPU参考指标-Acc | 硬件精度 | 内存 | 硬件利用率 |
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|bert_sst2|fp16|1x64|93.35|93.35|2168.6MB|78%|